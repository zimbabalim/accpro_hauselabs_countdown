Hause labs countdown

Demo: http://www.centrepede.com/DEV/accept-proceed/hauerlabs/countdown/5/_index.html
see below for debug directions

dev project dir: src - compiled resources will be written to 'dist' (css and js) - nothing here needs to be included with the release.
release dir: dist - these are the release files, include everything here for production.

Project requires to be run on a webserver/localhost, specifically due to XHR request (data/data.json).
JavaScript uses Requirejs for development and builds to minified and concatenated production code with Almondjs.
Please note comments above script tags in the html -
DEV line uncommented will run Requirejs project. Use this if you need to make changes to the js.
DEPLOY line uncommented will run the Almondjs release js
Obviously use only one or the other! See below for instructions to run the build script for release code.

CSS:
Compile SASS (from src/_.sass):
sass --watch ./hlcd-main.scss:../../dist/css/hlcd-main.css

Production JS (assuming Node installed):
Build from src/_.build:
node .\r.js -o .\build-script.js

Content:
'moments' are time triggered 'events' - in the current build there are two types of moment available: 'clock-message' and 'video'.
If you need to add another moment, please copy and paste an existing moment of the type that you need from 'dist/data/data.json', following correct JSON formatting.

For all types set a date and a time. Please ensure that the existing formatting is followed - there is no validation in place for this data, it is assumed that the data will be entered correctly. Specifically ensure that there is a dot between each pair, and that a leading zero is included in the pair if the value is less than ten, e.g:
"date"  :"17.07.2014",
"time"  :"15.45.00"

The spec requires that a 'moment' will be triggered once per day. The app will not allow concurrent moments, so please ensure that the moments defined do not overlap as the subsequent moment in an overlap will be ignored. In reality a moment could safely be triggered every few minutes, but with the above caveat.

Within a moment block in 'data.json' you should only need to change specific values. The uid value is only used for debugging purposes but it would be good practice to set this to a meaningful value. Aside from setting the time and date, below are use case for the current types of moment that the system is set up to recognise.

Use cases:
Clock message - set the desired message text in the 'text' block. This data is parsed as html. The system will calculate the message length and return to normal clock countdown once the message has finished displaying.

Video - The system will play an mp4 video with the html5 video tag. Place a video asset in 'assets/video', and declare it within 'uri'. The video width and height should be declared in 'widthHeight' (these are written as attributes into the generated video tag). Position the video accurately with the 'xy' property.

Debugging/view moments:
When running the app, append '?hlcddebug' to the url, i.e. '_index.html?hlcddebug'. This will show a panel with the uids of the created moments which can be clicked to run view the moment. As mentioned above moments can not run concurrently, so please wait for the current moment to complete before launching another one.

Moments can also be launched by typing into the console (Chrome, Safari etc) - this may be useful if screen capture is required without having the panel on screen. The command is:
m(index)
where 'index' is the number of the moment as declared in data.json, i.e. m(0) will play the first moment, m(1) will play the second and so forth.

You can also view the various clock events this way, with the command c(index). The clock events are:

c(0) = SHOW_PRE_KICKOFF_STATE (the state before countdown is scheduled to start)
c(1) = RUN_KICKOFF_ANIMATION (the state as soon as kickoff is triggered)
c(2) = SHOW_TIME (normal countdown)
c(3) = SHOW_EXPIRED_STATE (the state once targetDate is reached)

Time source:
Currently the app is using the client date() object. There are hooks in 'src/_.js/src/Bootstrap.js' to add server time or some webservice api time. If creating these, follow the format provided in the 'client' type and the system will function as expected.


