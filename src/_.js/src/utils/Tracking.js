/**
 * Zimbabalim
 * simple mechanism to output data that can be used by parent page for tracking purposes
 * they can do what they like via a custom jquery event
 */
 "use strict";

define([], function(){

    /**
     * @param _dto : object { type : string, params : array }
     */
    var dispatchTrackingEvent = function( _dto ){

        $( document ).trigger( "plc-event", _dto );
    };

    return{
        dispatchTrackingEvent:dispatchTrackingEvent
    }

} );
