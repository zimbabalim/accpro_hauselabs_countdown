/**
 * Zimbabalim
 * robust method to measure viewport
 */
 "use strict";

define([], function(){

    var call = function(){

        // NOTE this doesn't appear to work on android
        var test = document.createElement( "div" );

        test.style.cssText = "position: fixed;top: 0;left: 0;bottom: 0;right: 0;";
        document.documentElement.insertBefore( test, document.documentElement.firstChild );

        var w = test.offsetWidth,
            h = test.offsetHeight;

        document.documentElement.removeChild( test );

        // NOTE simple method
//        var h = parseInt( $(window).height(), 10),
//            w = parseInt( $(window).width(), 10);

        return { w:w, h:h };
    };

    return{
        get:call
    }



} );
