/**
 * Zimbabalim
 * generate flat key from date values to use for simple comparison operations
 */
 "use strict";

define([ "underscore" ], function( _ ){

    var createKey = function( _values_$ ){

        var key = "";

        _.each( _values_$, function( _v ){

            var vs = _v.toString();

            if( _v < 10 ){

                if( vs.length < 2 ){
                    vs = "0" + vs;
                }
            }

            key += vs + ".";
        });

        key = key.slice( 0, -1 ); // NOTE slice last dot - remove this if not using dots!

        //////console.log("/KEY GEN/ ===", key );

        return key;

    };

    return{
        createKey:createKey
    }

} );
