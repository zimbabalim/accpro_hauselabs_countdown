/**
 * Zimbabalim
 * abstract event names to allow equivalent actions dependant on platform
 */
 "use strict";

define([], function(){

    var useTouchEvents = null,
        events = null;

    /**
     * call this once to declare mode
     * @param _useTouchEvents : Boolean
     */
    var init = function( _useTouchEvents ){

        useTouchEvents = _useTouchEvents;

        if( useTouchEvents ){

            events = {
                mouseOverProxyName: "touchstart",
                mouseOutProxyName:"touchend",
                mouseMoveProxyName:"touchmove"
            }
        } else {
            events = {
                mouseOverProxyName:"mouseover",
                mouseOutProxyName:"mouseout",
                mouseMoveProxyName:"mousemove"
            };
        }
    };

    /**
     * any interested parties should call this to get the 'events' object to define binding names
     * @returns {*}
     */
    var getEvents = function(){
        return events;
    };

    return{
        useTouchEvents:useTouchEvents,
        events:events,
        init:init,
        getEvents:getEvents
    }

} );
