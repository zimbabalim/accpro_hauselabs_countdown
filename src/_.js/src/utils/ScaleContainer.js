/**
 * Zimbabalim
 */
 "use strict";

define([ "Config", "utils/ViewportDimensions" ], function( Config, ViewportDimensions ){

    var rootEl = null,
        hasScaledDown = false;

    var init = function( _rootEl ){

        rootEl = _rootEl;
        update();
    };


    var update = function(){

        var w = ViewportDimensions.get().w;

        if( w <= Config.viewportProps.minWidthBeforeScaling ){

            var sc = calc( w );

            scale( sc );
            hasScaledDown = true;

        } else if( hasScaledDown ) {
            scale( 1 );
        }


        /**
         *
         * @param _w
         */
        function calc( _w ){

            var sc = w / Config.viewportProps.minWidthBeforeScaling;

            ////console.log("CALC", _w, sc );

            return sc;
        }


        /**
         *
         * @param _sc
         */
        function scale( _sc ){


            // TODO add other prefixes
            rootEl.css({
                '-moz-transform': 'scale(' + _sc + ')',
                '-webkit-transform': 'scale(' + _sc + ')'

//                "moz-transform-origin": origin,
//                "-o-transform-origin": origin,
//                "-ms-transform-origin": origin,
//                "-webkit-transform-origin": origin,
//                "transform-origin": origin
            });

            /*
             -moz-transform: $transforms;
             -o-transform: $transforms;
             -ms-transform: $transforms;
             -webkit-transform: $transforms;
             transform: $transforms;
             */
        }
    };

    return{
        init:init,
        update:update
    }

} );
