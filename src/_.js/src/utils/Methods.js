/**
 * Zimbabalim
 */
 "use strict";

define([], function(){

    /**
     * get random number between ranges
     * @param _min
     * @param _max
     * @returns {number}
     */
    var rnd = function( _min, _max, _round ){

        _round = ( _round === undefined ) ? true : _round;

        var r = Math.random() * ( _max - _min ) + _min;
        r = ( _round ) ? Math.round( r ) : r;

        return r;

        //return Math.round( Math.random() * ( _max - _min ) + _min );
    };

    return{
        rnd:rnd
    }



} );
