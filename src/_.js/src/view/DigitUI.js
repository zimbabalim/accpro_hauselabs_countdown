/**
 * Zimbabalim
 */
 "use strict";

define([ "utils/Methods", "TweenMax", "Config", "signals" ], function( Methods, TweenMax, Config, signals ){


    function DigitUI(){

        var els = {
            el: null
        };


        var events = {
            Sg_ANIMATION_COMPLETED   : new signals.Signal()
        };


        var props = {
            index : null,
            role : null,
            currValue : 0,
            currFormattedValue : null,

            isFirstRun : true,
            initialKickoffState : null,

            isLocked : false
        };

        var currUpdateProps = null;

        /*var actions = {
            SHOW_PRE_KICKOFF_STATE:"SHOW_PRE_KICKOFF_STATE",
            RUN_KICKOFF_ANIMATION:"RUN_KICKOFF_ANIMATION",
            SHOW_EXPIRED_STATE:"SHOW_EXPIRED_STATE"
        };*/

        var actions = {

            SHOW_PRE_KICKOFF_STATE:{
                name:"SHOW_PRE_KICKOFF_STATE",
                hasFired:false
            },

            RUN_KICKOFF_ANIMATION:{
                name:"RUN_KICKOFF_ANIMATION",
                hasFired:false
            },

            SHOW_TIME:{
                name:"SHOW_TIME",
                hasFired:false
            },

            SHOW_EXPIRED_STATE:{
                name:"SHOW_EXPIRED_STATE",
                hasFired:false
            }

        };


        var init = function( _index, _role ){

            ////console.log("/digitui/ -init",  _index, _role );

            props.index = _index;
            props.role = _role;

            createEl();

        };


        var createEl = function(){

            els.el = $( document.createElement( "li" ) );
            els.el.addClass( "digit-element" );
            els.el.addClass( props.role );

            els.el.html("DIGIT ELEMENT");
        };

        var subscribe = function( _sg ){

            _sg.add( onSg );

            function onSg( _props ){
                //////console.log("/digitui/ -onSg", _props );

                switch ( _props.name ){

                    case "Sg_TIMER_UPDATED":

                        update( _props.data );
                        break;
                }
            }
        };


        var update = function( _props ){

            currUpdateProps = _props; // NOTE cache this
            ////console.log("DIGITUI has kicked off", _props );

            if( props.isFirstRun ){

                props.initialKickoffState = _props.hasKickedOff;
                props.isFirstRun = false;
            }

            checkState( _props );

            if( props.isLocked ){
                //console.log("/digitui/ DEFEAT -is locked");
                return;
            }

            props.currValue =  _props[ props.role ];
            props.currFormattedValue = formatValue( props.currValue );

            els.el.html( props.currFormattedValue );
        };


        var formatValue = function( _value ){

            _value = ( _value <= 0 ) ? 0 : _value; // NOTE prevent '-1' - we shouldn't ever get here in reality as timer will stop
            _value = ( _value < 10 ) ? ( "0" + _value ) : _value; // NOTE add leading zero

            return _value;
        };




        var checkState = function( _props ){

            var action = null;

            /**
             * hasn't kicked off - show initial state
             */
            if( !_props.hasKickedOff ){
                action = actions.SHOW_PRE_KICKOFF_STATE;
            }


            /**
             * has kicked off since we've been running
             */
            if( _props.hasKickedOff !== props.initialKickoffState ){
                action = actions.RUN_KICKOFF_ANIMATION;
            }


            /**
             * has kicked off since we've been running
             */
            if( _props.hasExpired ){
                action = action.SHOW_EXPIRED_STATE;
            }


            /************
             * run action
             */
            if( action && !action.hasFired ){
                cmdRunner( action, null );
            }
        };


        var cmdRunner = function( _action, _props ){

            //console.log("/digitui/ -cmdrunner", props.role, _action.name, _props );

            switch ( _action.name ){

                case actions.SHOW_PRE_KICKOFF_STATE.name:

                    props.isLocked = true;
                    els.el.html( "_" );

                    flashDigit( 3, 1 ); // TEST REMOVE

                    break;

                case actions.RUN_KICKOFF_ANIMATION.name:

                    props.isLocked = true;
                    kickOffAnimation();

                    break;

                case actions.SHOW_TIME.name:

                    props.isLocked = false;
                    flashDigit( 3, 0.2 );

                    break;

                case actions.SHOW_EXPIRED_STATE.name:

                    props.isLocked = true;
                    expiredStateAnimation();

                    break;
            }

            _action.hasFired = true;
        };



        var kickOffAnimation = function(){

            var //maxTm = Config.kickoffAnimationProps.maxTime,
                currTm = Config.kickoffAnimationProps.maxTime, // NOTE we want each digit to finish at more or less the same point
                hasTimedOut = false,
                isFirstRun = true;

            props.currValue = Methods.rnd( 0, 99 );

            animate();

            function animate(){

                var aProps = {
                    tm:Methods.rnd( 0.3, 1, false ),
                    pre:0,
                    toValue:Methods.rnd( 0, 99 )
                };


                if( isFirstRun ){
                    aProps.tm = 1.5; // NOTE longer lead in to start with
                    isFirstRun = false;
                }


                TweenMax.to( props, aProps.tm, { currValue:aProps.toValue,
                    ease:Sine.easeInOut,
                    onUpdate:onUpdate,
                    onComplete:onTweenComplete } );


                function onUpdate(){

                    props.currValue = Math.ceil( props.currValue );

                    if( props.currValue <= 0 ){
                        props.currValue = 0;
                    }

                    if( props.currValue >= 99 ){
                        props.currValue = 99;
                    }

                    els.el.html( formatValue( props.currValue ) ); // NOTE update ui

                    if( currTm <= 0 ){
                        onAnimationComplete();
                    }
                }

                function onTweenComplete(){

                    currTm -= aProps.tm;
                    ( !hasTimedOut ) ? animate() : null;
                }


                function onAnimationComplete(){

                    hasTimedOut = true;
                    TweenMax.killTweensOf( props.currValue );

                    // *********************************
                    //cmdRunner( actions.SHOW_TIME, null );
                    events.Sg_ANIMATION_COMPLETED.dispatch( { name:"Sg_ANIMATION_COMPLETED", data:actions.RUN_KICKOFF_ANIMATION.name + "_COMPLETE" } );
                    // *********************************
                }
            }
        };



        /**
         * this one had problems - leaving in for reference
         */
/*
        var kickOffAnimation = function(){

            //console.log("KICK OFF ANIMATION");

            // NOTE ascii ranges
            var numericRange_$ = [ 48, 57 ], // NOTE 0-9
                alphaRange_$ = [ 65, 90 ]; // NOTE A-Z (caps only)

            var modulo = 5,
                tick = 0,
                maxTicks = 100,
                direction = 1;

            var currValue = Methods.rnd( 0, 99 );

            var RAF;// = window.requestAnimationFrame( update );

            update();


            function update(){

                // TODO
                if( tick >= maxTicks ){

                    //console.log("FINISHED");

                    window.cancelAnimationFrame( RAF );
                    // FIRE EVENT

                    return;
                }

                ////console.log("@@@", tick % modulo );


                // NOTE random change in direction
                var max1 = 500,
                    rnd1 = Methods.rnd( 1, max1 );

                if( rnd1 >= ( max1 - 1 ) ){
                    direction = ( direction - ( direction * 2 ) );

                }

                // NOTE random change in modulus
                var max2 = 50,
                    rnd2 = Methods.rnd( 1, max2 );

                if( rnd2 >= ( max2 - 1 ) ){
                    modulo = Methods.rnd( 1, 10 );
                }


                // NOTE random change in increment value
//                var max3 = 100,
//                    rnd3 = Methods.rnd( 1, max3 );
//
//                if( rnd3 >= ( max3 - 1 ) ){
//                    //console.log("CHANGE");
//
//                    if( currValue > 25 && currValue < 75 ){
//                        direction += Methods.rnd( 1, 2 );
//                    }
//                }


                // NOTE random change in value
                var max4 = 100,
                    rnd4 = Methods.rnd( 1, max4 );

                if( rnd4 >= ( max4 - 1 ) ){
                    currValue = Methods.rnd( 5, 95 );
                }


                if( tick % modulo === 0 ){


                    // NOTE if zero and counting down
                    if( currValue === 0 && direction === -1 ){
                        direction = 1;
                    }

                    // NOTE if 99 and counting up
                    if( currValue === 99 && direction === 1 ){
                        direction = -1;
                    }


                    currValue += direction;

                    if( currValue <= 0 ){
                        currValue = 0;
                    }

                    if( currValue >= 99 ){
                        currValue = 99;
                    }


                    els.el.html( formatValue( currValue ) );
                }

                RAF = window.requestAnimationFrame( update, null );
                tick ++;
            }



        };
*/



        var expiredStateAnimation = function(){
            //console.log("EXPIRED ANIMATION");

            els.el.html( formatValue( 0 ) );
            flashDigit( 9999, 1 );
        };



        var flashDigit = function( _numTimes, _tm, _stagger ){

            //console.log("-----flash text----");

            var currCount = 0;

            flash();

            function flash(){

                if( currCount === _numTimes ){

                    //console.log("FLASH COMPLETE");
                    return;
                }

                TweenMax.to( els.el, _tm / 8, { css:{ opacity:0 }, delay:0 } );

                TweenMax.to( els.el, _tm, { css:{ opacity:1 }, delay:_tm / 8, onComplete:function(){
                    flash();
                    currCount ++;
                } }  )

            }
        };



        return{
            els         : els,
            events      : events,
            actions     : actions,
            init        : init,
            cmdRunner   : cmdRunner,
            subscribe   : subscribe
        }

    }

    return DigitUI;

} );
