/**
 * Zimbabalim
 */
"use strict";

define([ "TweenMax", "signals", "Config" ],
    function( TweenMax, signals, Config ){

        function VideoPlayerUI(){

            var els = {
                el  : null
            };

            var events = {
                Sg_ANIMATION_COMPLETED  : new signals.Signal()
            };

            var vo = null;

            var init = function( _contentVO ){

                ////console.log("/videoplayerui/ -init", _contentVO );

                vo = _contentVO;

                createElement();
                reset();
            };


            var createElement = function(){

                var wh_$ = vo.widthHeight.split( ","),
                    xy_$ = vo.xy.split( ",");

                ////console.log("PROPS", wh_$, xy_$ );

                els.el = $( document.createElement( "video" ) );
                els.el.addClass( "video-player" );

                els.el.css( {
                    top: parseInt( xy_$[ 0 ], 10 ),
                    left:parseInt( xy_$[ 1 ], 10 )
                } );

                els.el[ 0 ].setAttribute( "width", wh_$[ 0 ] );
                els.el[ 0 ].setAttribute( "height", wh_$[ 1 ] );

                els.el[ 0 ].setAttribute( "src", Config.paths.videoDir + vo.uri );

                // #

            };


            var reset = function(){
                TweenMax.to( els.el, 0, { css:{ opacity:0 } } );

                els.el.on( "ended", function() {
                    ////console.log("VID END");
                    dispatchNotification( events.Sg_ANIMATION_COMPLETED, { name:"Sg_ANIMATION_COMPLETED", data:null });
                });

                els.el.load();
            };


            var show = function(){

                /*TweenMax.to( els.el, 1, { css:{ opacity:1 } } );*/

                els.el.on( "timeupdate", function() {

                    ////console.log("VID HAS STARTED");
                    TweenMax.to( els.el, 1, { css:{ opacity:1 }, delay:0.5 } );
                });


                els.el[ 0 ].play();
            };


            // #

            var subscribe = function( _sg ){

                _sg.add( onSg );

                function onSg( _props ){

                    ////console.log( "/clockmessageui/ onSg", _props.name );

                    switch ( _props.name ){

                        case "Sg_SHOW_START":

                            show(); // FIXIT
                            break;

                        case "Sg_SHOW_END":

                            break;

                        case "Sg_HIDE_START":

                            break;

                        case "Sg_HIDE_END":

                            break;
                    }
                }
            };


            var dispatchNotification = function( _sg, _props ){

                _sg.dispatch( _props );
            };



            return{
                els         :els,
                events      :events,
                init        :init,
                subscribe   :subscribe
            }
        }

        return VideoPlayerUI;

    } );
