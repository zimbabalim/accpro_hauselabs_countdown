/**
 * Zimbabalim
 */
"use strict";

define([ "signals", "TweenMax", "view/ClockMessageUI", "view/VideoPlayerUI" ],
    function( signals, TweenMax, ClockMessageUI, VideoPlayerUI ){

        function MomentUI(){

            var els = {
                el      : null
            };


            var events = {

                Sg_SHOW_START   : new signals.Signal(),
                Sg_SHOW_END     : new signals.Signal(),
                Sg_HIDE_START   : new signals.Signal(),
                Sg_HIDE_END     : new signals.Signal(),

                Sg_MOMENT_REQUEST   : new signals.Signal()
            };


            var props = {
                type    : null
            };

            var mixinProps = {

                onShowAction    : null,
                onHideAction    : null
            };


            var vo = null,
                MIXIN = null;

            var init = function( _momentVO ){

                vo = _momentVO;
                els.el = $( "." + vo.content.el );

                props.type = vo.content.type;

                initMixin();
                reset();
            };


            /**
             * see data.json for type names
             * TODO hold these as keys somewhere?
             */
            var initMixin = function(){

                ////console.log("TYPE", props.type );

                switch( props.type ){

                    case "clock-message":

                        MIXIN = new ClockMessageUI();
                        MIXIN.init( vo.content );

                        mixinProps.onShowAction = "HIDE_DIGITS";
                        mixinProps.onHideAction = "SHOW_DIGITS";

                        break;


                    case "video":

                        MIXIN = new VideoPlayerUI();
                        MIXIN.init( vo.content );

                        break;


                    case "png-sequence":

                        break;
                }


                if( !MIXIN ){
                    ////console.log("MIXIN DEFEAT");
                    return;
                }

                MIXIN.subscribe( events.Sg_SHOW_START );
                MIXIN.subscribe( events.Sg_SHOW_END );
                MIXIN.subscribe( events.Sg_HIDE_START );
                MIXIN.subscribe( events.Sg_HIDE_END );

                MIXIN.events.Sg_ANIMATION_COMPLETED.addOnce( onMixinAnimationCompleted );

                els.el.append( MIXIN.els.el );

                ////console.log("============", MIXIN.els.el );
            };


            var onMixinAnimationCompleted = function(){
                ////console.log("MIXIN COMPLETED");

                TweenMax.delayedCall( vo.content.hideDelay, hide );
            };


            var reset = function(){

                els.el.css( { opacity:0 } );
            };

            var show = function(){

                TweenMax.to( els.el, 1, { css:{ opacity:1 },
                    onStart:onShowStart, onComplete:onShowComplete } );

                function onShowStart(){

                    ////console.log("MOMENT SHOW -start");

                    els.el.css( { "display":"block" } );

                    dispatchNotification(
                        events.Sg_SHOW_START,
                        { name:"Sg_SHOW_START", data:{ cmd:mixinProps.onShowAction } }
                    );
                }

                function onShowComplete(){

                    //////console.log("MOMENT SHOW -end");

                    dispatchNotification(
                        events.Sg_SHOW_END,
                        { name:"Sg_SHOW_END", data:null }
                    );
                }
            };

            var hide = function(){

                ////console.log("HIDE MOMENT!!!!");

                //return; // FIXIT REMOVE - debug prevent hide

                TweenMax.to( els.el, 1, { css:{ opacity:0 },
                    onComplete:onHideComplete } );


                function onHideComplete(){

                    ////console.log("MOMENT HIDE -end");

                    dispatchNotification(
                        events.Sg_HIDE_END,
                        { name:"Sg_HIDE_END", data:{ cmd:mixinProps.onHideAction } }
                    );
                }

            };


            var dispose = function(){

                ////console.log("DISPOSED" );
//                $( els.el ).remove( MIXIN.els.el );

                MIXIN.els.el.remove();

            };


            var update = function(){

            };


            var dispatchNotification = function( _sg, _props ){

                _sg.dispatch( _props );
            };

            return {
                els     : els,
                events  : events,
                init    : init,
                show    : show,
                dispose : dispose
            }

        }

        return MomentUI;

    } );
