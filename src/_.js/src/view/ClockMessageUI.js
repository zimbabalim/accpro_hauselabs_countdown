/**
 * Zimbabalim
 */
"use strict";

define([ "TweenMax", "signals", "Config" ], function( TweenMax, signals, Config ){

    function ClockMessageUI(){

        var els = {
            el          : null,
            messageEl   : null
        };

        var events = {
            Sg_ANIMATION_COMPLETED  : new signals.Signal()
        };

        var props = {

            speed       :Config.clockMessageProps.speed,
            modulo      : (Config.clockMessageProps.modulo >= 1 ) ? Config.clockMessageProps.modulo : 1,

            startX    : null,
            endX      : null,
            currX     : null,

            messageElReady  : false,
            promiseShow     : false,

            tick        : 0
        };

        var textHTML = null;

        var init = function( _contentVO ){

            textHTML = _contentVO.text;
            createElement();
            //setProps();
            reset();

            ////console.log("/clockmessageui/ -init", textHTML )
        };


        var createElement = function(){

            els.el = $( document.createElement( "div" ) );
            els.el.addClass( "message-container" );

            els.messageEl = $( document.createElement( "div" ) );
            els.messageEl.addClass( "message" );

            els.el.append( els.messageEl );
            els.messageEl.html( textHTML );


            /**
             * 'mutation observer' poo
             * @type {number}
             */
            var interval = window.setInterval( poll, 20 );

            function poll(){

                var r = els.messageEl[ 0 ].getBoundingClientRect();

                if( r.width > 0 ){

                    window.clearInterval( interval );
                    props.messageElReady = true;
                    setProps( r );
                }
            }
        };


        var setProps = function( _rect ){

            ////console.log( "SET PROPS" );

            //var r = els.messageEl[ 0 ].getBoundingClientRect();

            ////console.log("MESSAGE RECT", _rect );

            props.startX = ( _rect.left ) - ( _rect.left / 5 );
            props.endX = -( _rect.width );

            if( props.promiseShow ){
                ////console.log("PROMISE SHOW ---");
                show();
            }

        };


        var reset = function(){

            TweenMax.to( els.el, 0, { css:{ opacity:0 } } );

            //els.messageEl.css( { left:props.startX } );
        };

        var show = function(){

            if( !props.messageElReady ){
                ////console.log("MESSAGE DEFEAT");
                props.promiseShow = true;
                return;
            }

            ////console.log("SHOW MESSAGE");

            TweenMax.to( els.el, 1, { css:{ opacity:1 } } );
            scrollText();
        };

        var scrollText = function(){

            props.currX = props.startX;

            scroll();

            function scroll(){

                if( props.currX <= props.endX ){

                    ////console.log("SCROLL FINISHED");
                    window.cancelAnimationFrame( null );

                    dispatchNotification( events.Sg_ANIMATION_COMPLETED, { name:"Sg_ANIMATION_COMPLETED", data:null });

                    return;
                }

                /**
                 * modulo updates to spare poor mobile devices
                 */
                if( props.tick % props.modulo === 0 ){
                    //////console.log("scroll", props.tick, props.modulo );

                    var xs = Math.ceil( props.currX ).toString() + "px";
                    els.messageEl[ 0 ].style.left = xs;

//                els.messageEl.css( {
//                    "-webkit-transform" : "translate( " + xs +", 0px )"
//                } );

                    props.currX -= props.speed;
                }

                window.requestAnimationFrame( scroll, null );
                props.tick ++;
            }

        };


        // #

        var subscribe = function( _sg ){

            _sg.add( onSg );

            function onSg( _props ){

                ////console.log( "/clockmessageui/ onSg", _props.name );

                switch ( _props.name ){

                    case "Sg_SHOW_START":

                        show(); // FIXIT
                        break;

                    case "Sg_SHOW_END":

                        break;

                    case "Sg_HIDE_START":

                        break;

                    case "Sg_HIDE_END":

                        break;
                }
            }
        };


        var dispatchNotification = function( _sg, _props ){

            _sg.dispatch( _props );
        };


        return{
            els         :els,
            events      :events,
            init        :init,
            subscribe   :subscribe
        }
    }

    return ClockMessageUI;

} );
