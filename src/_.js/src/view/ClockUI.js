/**
 * Zimbabalim
 */
 "use strict";

define([ "signals", "TweenMax", "view/DigitUI" ], function( signals, TweenMax, DigitUI ){

    function ClockUI( _el ){

        var els = {
            el                      : _el,
            digitsContainerEl       : _el.find( ".digits-container" )
        };

        var props = {
            roles_$     : [ "days", "hours", "minutes", "seconds" ],
            numDigits   : null,
            digitAnimationsCompleted : 0
        };

        var uis = {
            digitUIs_$  : []
        };

        var events = {
            Sg_TIMER_UPDATED : new signals.Signal()
        };

        var actions = {

            /*SHOW_PRE_KICKOFF_STATE:"SHOW_PRE_KICKOFF_STATE",
            RUN_KICKOFF_ANIMATION:"RUN_KICKOFF_ANIMATION",
            SHOW_EXPIRED_STATE:"SHOW_EXPIRED_STATE",*/

            HIDE_DIGITS : "HIDE_DIGITS",
            SHOW_DIGITS : "SHOW_DIGITS"
        };

        // #

        var init = function(){

            ////console.log("/clockui/ -init" );

            props.numDigits = props.roles_$.length;

            for( var i = 0; i < props.numDigits; i ++ ){

                var role = props.roles_$[ i ],
                    dui = new DigitUI();

                dui.init( i, role );
                dui.subscribe( events.Sg_TIMER_UPDATED );
                dui.events.Sg_ANIMATION_COMPLETED.add( onDigitAnimationCompleted );

                uis.digitUIs_$.push( dui );
                els.digitsContainerEl.append( dui.els.el );
            }
            //TweenMax.to( els.el, 5, { css:{ perspective:200 } } );
        };



        var update = function( _timerProps ){

            //console.log("/clockui/ -update", _timerProps );

            var vo = {
                name:"Sg_TIMER_UPDATED",
                data:_timerProps
            };

            events.Sg_TIMER_UPDATED.dispatch( vo );
        };



        var cmdRunner = function( _cmd, _props ){

            switch( _cmd ){

                case actions.HIDE_DIGITS:

                    TweenMax.to( els.digitsContainerEl, 1, { css:{ opacity:0 }, delay:_props.delay } );

                    break;

                case actions.SHOW_DIGITS:

                    TweenMax.to( els.digitsContainerEl, 1, { css:{ opacity:1 } } );

                    break;
            }
        };



        // NOTE getting a little rushed here
        var onDigitAnimationCompleted = function( _props ){

            props.digitAnimationsCompleted ++;

            if( props.digitAnimationsCompleted === props.numDigits ){

                props.digitAnimationsCompleted = 0;
                //console.log("ANIMS COMPLETED");

                switch( _props.data ){

                    case "RUN_KICKOFF_ANIMATION_COMPLETE":
                        digitCmdRunnerProxy( 2 );
                        break;

                }
            }
        };





        var digitCmdRunnerProxy = function( _index ){

            var actions = uis.digitUIs_$[ 0 ].actions, // NOTE get actions from one of the instantiated uis
                currAction = null;

            switch ( _index ){

                case 0:
                    currAction = actions.SHOW_PRE_KICKOFF_STATE;
                    break;

                case 1:
                    currAction = actions.RUN_KICKOFF_ANIMATION;
                    break;

                case 2:
                    currAction = actions.SHOW_TIME;
                    break;

                case 3:
                    currAction = actions.SHOW_EXPIRED_STATE;
                    break;
            }

            for( var i = 0; i < props.numDigits; i ++ ){

                var dui = uis.digitUIs_$[ i ];
                dui.cmdRunner( currAction, null );
            }
        };


        /**
         * DEBUG ONLY
         * @param _index
         */
        window.c = function( _index ){

            digitCmdRunnerProxy( _index );
        };





        return {
            actions:actions,
            init:init,
            update:update,
            cmdRunner:cmdRunner
        }

    }

    return ClockUI;

} );
