/**
 * Zimbabalim
 */
"use strict";

define([ "Config", "Model", "utils/SmartResize", "utils/ScaleContainer", "managers/TimeEngine", "managers/MomentsManager", "view/ClockUI", "view/MomentUI" ],
    function( Config, Model, SmartResize, ScaleContainer, TimeEngine, MomentsManager, ClockUI, MomentUI ){


        var els = {
            rootEl              :null,
            spinnerEl           :null,
            clockEl             :null,
            momentsContainerEl  :null
        };

        var uis = {
            clockUI             :null,
            currMomentUI        :null
        };

        var props = {

            isMomentRunning     :false
        };


        var init = function(){

            ////console.log("/viewmediator/ -init" );

            els.rootEl = $( "#hlcd-container" );
            els.spinnerEl = els.rootEl.find( ".spinner" );
            els.clockEl = els.rootEl.find( ".clock" );
            els.momentsContainerEl = els.rootEl.find( ".moments-container" );

            uis.clockUI = new ClockUI( els.clockEl );
            uis.clockUI.init();

            ScaleContainer.init( els.rootEl );

            // NOTE update on debounced resize
            $( window ).smartresize( function(){
                ScaleContainer.update();
            });

            initManagers();

            // NOTE show debug panel?
            if( Config.startupProps.enableDebugQueryString ){
                ( window.location.search.substring(1) === "hlcddebug" ) ? showDebug() : null;
            }
        };



        var initManagers = function(){

            MomentsManager.events.Sg_MOMENT_ACTIVATED.add( onMomentActivated );
            MomentsManager.init( Model.getVObyKey( Model.voKeys.momentsVO ).data/*.dateObjects_$*/  );

            TimeEngine.events.Sg_TIME_ENGINE_UPDATE.add( onTimeEngineUpdate );

            var currentTimeVO = Config.timeAndDateProps.dateObject,
                targetTimeVO = Model.getVObyKey( Model.voKeys.targetDateTimeVO ).data.dateObject,
                kickoffTimeVO = Model.getVObyKey( Model.voKeys.kickoffDateTimeVO ).data.dateObject;

            ////console.log( "***",currentTimeVO, targetTimeVO, kickoffTimeVO );

            /*TimeEngine.init(
             Config.timeAndDateProps.dateObject,
             Model.getVObyKey( Model.voKeys.targetDateTimeVO ).data.dateObject
             );*/

            TimeEngine.init( currentTimeVO, targetTimeVO, kickoffTimeVO );
        };


        var test = 0;
        var onTimeEngineUpdate = function( _props ){

            uis.clockUI.update( _props );
            MomentsManager.update( _props );
        };


        /**
         * on 'Sg_MOMENT_ACTIVATED' create new MomentUI
         * @param _momentVO
         */
        var onMomentActivated = function( _momentVO ){

            if( props.isMomentRunning ){
                ////console.log("DEFEAT MOMENT");
                return;
            }

            ////console.log("/viewmediator/ -onMomentActivated", _momentVO );

            // TODO test for is there a momentui already here?

            uis.currMomentUI = new MomentUI();
            uis.currMomentUI.init( _momentVO.data );
            uis.currMomentUI.show();

            uis.currMomentUI.events.Sg_SHOW_START.addOnce( cmdRunner );
            uis.currMomentUI.events.Sg_HIDE_END.addOnce( cmdRunner );

            props.isMomentRunning = true;
        };


        var cmdRunner = function( _sg ){

            ////console.log("-----CMD RUNNER", _sg.name, _sg.data.cmd );

            switch ( _sg.name ){

                case "Sg_HIDE_END":
                    uis.currMomentUI.dispose(); // NOTE remove mixin from moment
                    props.isMomentRunning = false;
                    break;
            }

            // ..................................................
            // ..................................................

            switch ( _sg.data.cmd ){

                case "HIDE_DIGITS":

                    uis.clockUI.cmdRunner( _sg.data.cmd, { delay: 0 } );
                    break;

                case "SHOW_DIGITS":

                    uis.clockUI.cmdRunner( _sg.data.cmd, null );
                    break;
            }
        };



        var showDebug = function(){

            ////console.log("DEBUG");

            var moments_$ =  Model.getVObyKey( Model.voKeys.momentsVO ).data,
                containerEl = createElement( "hlcddebug" );

            $( "body" ).append( containerEl );

            for( var i = 0; i < moments_$.length; i++ ){

                var el = createElement( null, i + 1 );

                el.html( moments_$[ i ].uid );
                el.on( "click", function(){

                    var idx = parseInt( $( this ).data( "index" ), 10 ) - 1;
                    window.m( idx )
                } );

                containerEl.append( el );
            }





            var clockEffects_$ = [
                "SHOW_PRE_KICKOFF_STATE",
                "RUN_KICKOFF_ANIMATION",
                "SHOW_TIME",
                "SHOW_EXPIRED_STATE"
            ];

            for( var j = 0; j < clockEffects_$.length; j ++ ){

                console.log("!!!");

                var idx = ( j + moments_$.length ) + 1,
                    el2 = createElement( null, idx );

                el2.text( clockEffects_$[ j ] );
                el2.on( "click", function(){

                    var idx = parseInt( $( this ).data( "index" ), 10 ) - 1;
                    window.c( idx - moments_$.length );
                } );

                containerEl.append( el2 );

            }


            function createElement( _className, _index ){

                var el = $( document.createElement( "div" ) );
                ( _className ) ? el.addClass( _className ) : null;
                ( _index ) ? el.attr('data-index', _index ) : null;

                return el;
            }
        };


        return{
            init:init
        }


    } );
