/**
 * Zimbabalim
 * Load data file, feed model, write capabilities and force cacheing of declared images if appropriate
 * - this class must fire Sg_COMPLETE before app will continue to initialise
 */
"use strict";


define([ "Config", "Model", "signals", "utils/MouseTouch", "utils/ViewportDimensions" ],
    function( Config, Model, signals, MouseTouch, ViewportDimensions ){

        var Sg_COMPLETE = new signals.Signal(),
            numTasks = 0,
            numTasksCompleted = 0;

        var init = function(){

            getData();
            getTimeAndDate();
            mixinCapabilitiesData( cacheImages );
        };



        /**
         * load json
         */
        var getData = function(){

            numTasks ++;

            //var url = "data/" + lang + "/data.json";

            var url = Config.paths.data + cacheBuster();

            $.ajax({
                type:"GET",
                url:url,
                dataType:"json",

                success:function( r ){
                    onResult( true, r );
                },

                error: function(){
                    onFail();
                }
            });


            function cacheBuster(){
                return "?" + ( new Date ).getTime();
            }
        };


        var onResult = function( _status, _data ){

            ////console.log( "/bootstrap/ -onResult", _status, _data );

            Model.Sg_READY.addOnce( function(){

                //console.log(">>> target", Model.getVObyKey( Model.voKeys.targetDateTimeVO ));
                //console.log(">>> kickoff", Model.getVObyKey( Model.voKeys.kickoffDateTimeVO ));
                //console.log(">>> moments", Model.getVObyKey( Model.voKeys.momentsVO ));

                taskCompleted( "model-ready" );
            });

            Model.setData( _data );
        };


        var onFail = function(){
            console.error( "/bootstrap/ -onfail" );
        };




        /**
         * list images to be cached
         * - note this will be skipped if mobile/ie8 as the parallax carousel component is not used
         */
        var cacheImages = function(){

            //////console.log("/bootstrap/ -cacheimages");

            numTasks ++;

            var
                forcePass = false,
                basePath,
                paths_$ = [

                ],
                numImages = paths_$.length,
                numLoaded = 0;

            getImagePath(); // NOTE attempt to extract image path

            if( numImages === 0 || forcePass ){
                ////console.log("cacheImages - skip", numImages, forcePass );
                taskCompleted( "cacheImages" );
                return;
            }


            for( var i = 0; i < numImages; i ++ ){

                var image = document.createElement('img');

                image.src = basePath + paths_$[ i ];
                image.onload = function(){

                    numLoaded ++;
                    ////console.log("> loaded", numLoaded, "/", numImages );

                    if( numLoaded === numImages ){
                        taskCompleted( "cacheImages" );
                    }
                }
            }


            /**
             * use an element with background-image property e.g. 'spinner' to determine the
             * base path of images to be cached
             * if element not found or if it doesn't have background-image set, set forcePass true to skip caching
             */
            function getImagePath(){

                var testEl = $(".spinner");

                // NOTE quit - no element
                if( !testEl.length ){
                    forcePass = true;
                    return;
                }

                var css = testEl[ 0 ].currentStyle || window.getComputedStyle(testEl[ 0 ], false ),
                    url = css.backgroundImage.slice(4, -1);

                // NOTE quit - element doesn't have a background image
                if( url.length === 0 ){
                    forcePass = true;
                }

                // NOTE run this twice to go back two steps
                basePath = beforeLast( url, "/" );
                basePath = beforeLast( basePath, "/" ) + "/";

                Config.paths.images = basePath; // NOTE may as well keep this somewhere

                // NOTE string util
                function beforeLast( p_string, p_char ) {
                    if (p_string == null) { return ''; }
                    var idx = p_string.lastIndexOf(p_char);
                    if (idx == -1) { return ''; }
                    return p_string.substr(0, idx);
                }
            }
        };


        /**
         * add capabilities props to model > configvo
         * best to just get it out of the way now
         * @param _fn :: callback
         */
        var mixinCapabilitiesData = function( _fn ){

            numTasks ++;

            Config.viewportProps.dims = ViewportDimensions.get();
            Config.capabilities.isDaftClient = $( "html" ).hasClass( "lt-ie9" );
            Config.capabilities.isMobile = ( Config.viewportProps.dims.w <= Config.viewportProps.mobileBreakpoint );
            Config.capabilities.useTouchEvents = (( 'ontouchstart' in window ) || ( navigator.msMaxTouchPoints > 0 ));

            MouseTouch.init( Config.capabilities.useTouchEvents );

            // NOTE only cache images if not mobile view or ie8
            // NOTE nesting conditionals as ie8 seems to ignore bitwise OR.
            if( !Config.capabilities.isMobile ){

                if( !Config.capabilities.isDaftClient && _fn ){

                    _fn(); // NOTE callback
                }
            }

            //////////console.log("/bootstrap/ --viewport", Config.viewportProps );
//            //////console.log("/bootstrap/ -capabilities", Config.capabilities );

            taskCompleted( "mixinCapabilitiesData" );
        };


        // ................................................................................
        // ................................................................................
        // ................................................................................


        var getTimeAndDate = function(){

             switch ( Config.timeAndDateProps.method ){

                 case "client":
                        useClient();
                     break;


                 case "server":
                        useServer();
                     break;


                 case "api":
                        useAPI();
                     break;

             }

            // FIXIT IMPORTANT! don't use this method for production!
            function useClient(){

                var date = new Date(),
                    vo = {
                        yy:date.getFullYear(),
                        mm:date.getMonth() + 1,
                        dd:date.getDate(),
                        hour:date.getHours(),
                        mins:date.getMinutes(),
                        secs:date.getSeconds()
                    };

                setValues( vo );
            }


            // TODO
            function useServer(){

            }


            // TODO
            function useAPI(){

            }

            /**
             * write values to config - parse these from whichever method used before this is hit
             * @param _vo
             */
            function setValues( _vo ){

                Config.timeAndDateProps.dateObject = {

                    yy      : s( _vo.yy ),
                    mm      : s( _vo.mm ),
                    dd      : s( _vo.dd ),
                    hour    : s( _vo.hour ),
                    mins    : s( _vo.mins ),
                    secs    : s( _vo.secs )
                };

                /**
                 * convert to string
                 * @param _v
                 * @returns {*|string}
                 */
                function s( _v ){
                    return _v.toString();
                }

                ////console.log("---- /bootstrap/ -date", Config.timeAndDateProps );
            }


            // FIXIT use split instead
//            function parseValues( _values_$ ){
//
//                var s = "";
//
//                for( var i = 0; i < _values_$.length; i ++ ){
//
//                    var v = _values_$[ i ];
//                    s += v.toString() + "."
//                }
//
//                s = s.slice( 0, - 1 ); // NOTE trim last dot
//
//                return s;
//            }
        };


        // ................................................................................
        // ................................................................................
        // ................................................................................











        /**
         * count up tasks completed and fire Sg_COMPLETE if all done
         * @param _taskName
         */
        var taskCompleted = function( _taskName ){

            numTasksCompleted ++;
            ////console.log("/bootstrap/ -taskCompleted", numTasksCompleted, "/", numTasks, _taskName );

            if( numTasksCompleted === numTasks ){
                ////console.log("/bootstrap/ DONE" );
                Sg_COMPLETE.dispatch();
            }
        };



        return{
            Sg_COMPLETE: Sg_COMPLETE,
            init: init
        }

    } );
