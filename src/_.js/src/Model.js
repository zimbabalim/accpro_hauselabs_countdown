/**
 * Zimbabalim
 * repository for parsed input data
 */
"use strict";

define([ "signals", "underscore", "Config", "utils/WriteDateObjKey" ],
    function( signals, _, Config, WriteDateObjKey ){

        var Sg_READY = new signals.Signal(),
            rawData,
            vos_$;

        var voKeys = {
            targetDateTimeVO    :"targetDateTimeVO",
            kickoffDateTimeVO     :"kickoffDateTimeVO",
            momentsVO           :"momentsVO"
        };

        /**
         * public method fed with json data
         * @param _data
         */
        var setData = function( _data ){

            vos_$ = [];
            rawData = _data[ 0 ];

            parseData();
        };


        /**
         * split data into vos
         */
        var parseData = function(){

            var spoof = false;

            if( spoof ){

                var spoofData = createSpoofData();

                //console.log("!!!! SPOOF DATA !!!!", spoofData );

                createVO( voKeys.kickoffDateTimeVO, spoofData[ 0 ], filterVO );
                createVO( voKeys.targetDateTimeVO, spoofData[ 1 ], filterVO );

            } else {
                createVO( voKeys.kickoffDateTimeVO, rawData[ "kickoffDateTime" ], filterVO );
                createVO( voKeys.targetDateTimeVO, rawData[ "targetDateTime" ], filterVO );
            }

            //return; // FIXIT REMOVE!!!!



            /*createVO( voKeys.targetDateTimeVO, rawData[ "targetDateTime" ], filterVO );
             createVO( voKeys.kickoffDateTimeVO, rawData[ "kickoffDateTime" ], filterVO );*/
            createVO( voKeys.momentsVO, rawData[ "moments" ], filterMomentsVO );

            function createVO( _name, _data, _filterMethod ){

                var vo = {
                    name:_name,
                    data:_data
                };

                ( _filterMethod ) ?_filterMethod( vo ) : null;

                vos_$.push( vo );
            }


            function filterVO( _vo ){

                _vo.data.dateObject = parseDateObject( _vo.data.date, _vo.data.time );
                _vo.data.dateObjectKey = WriteDateObjKey.createKey( _vo.data.dateObject );  // FIXIT RESTORE

                ////console.log("filterVO",  _vo.data.dateObject, _vo.data.dateObjectKey );
            }


            function filterMomentsVO( _vo ){

                var dateObjects_$ = [];

                for( var i = 0; i < _vo.data.length; i ++ ){

                    var m = _vo.data[ i ],
                        dateObjectVO = {};

                    dateObjectVO.raw = parseDateObject( m.date, m.time );
                    dateObjectVO.dateObjectKey = WriteDateObjKey.createKey( dateObjectVO.raw ); // FIXIT RESTORE

                    dateObjects_$.push( dateObjectVO );
                }

                _vo.data.dateObjects_$ = dateObjects_$;

                ////console.log("filterMomentsVO",  _vo.data.dateObjects_$ );
            }


            /**
             * utility method. ensure data is supplied in correct format!
             */

            function parseDateObject( /*_date_$, _time_$*/ _date, _time ){

                var _date_$ = _date.split( "." ),
                    _time_$ = _time.split( "." );

                var dateObj = {
                    yy      : n( _date_$[ 2 ] ),
                    mm      : n( _date_$[ 1 ] ),
                    dd      : n( _date_$[ 0 ] ),
                    hour    : n( _time_$[ 0 ] ),
                    mins    : n( _time_$[ 1 ] ),
                    secs    : n( _time_$[ 2 ] )
                };

                /**
                 * convert to number
                 * @param _v
                 * @returns {*}
                 */
                function n( _v ){
                    return _v; // NOTE or not...
                    //return parseInt( _v, 10 );
                }

                return dateObj;
            }


            Sg_READY.dispatch();
        };


        /**
         * public method for retrieval of value object
         * - see 'voKeys' object for string names
         * @param _name
         * @returns {*}
         */
        var getVObyKey = function( _name ){

            var vo = _.find( vos_$, function( _vo ){

                _vo = ( _vo.name === _name ) ? _vo : null;
                return _vo;
            } );

            return vo;
        };


        /**
         * FIXIT not working correctly - don't use in this state
         * @returns {*[]}
         */
        var createSpoofData = function(){

            var secsOffset = 12;

            var cdo = Config.timeAndDateProps.dateObject,

                kodate = writeString( [ cdo.dd, cdo.mm, cdo.yy ] ),
                kotime = writeString( [ cdo.hour, cdo.mins, parseInt( cdo.secs, 10 ) - secsOffset ]),

                tgdate = writeString( [ cdo.dd, cdo.mm, cdo.yy ] ),
                tgtime = writeString( [ cdo.hour, cdo.mins, parseInt( cdo.secs, 10 ) + secsOffset ] );

            var spoofKickoff = {
                date: kodate,
                time: kotime
            };

            var spoofTarget = {
                date: tgdate,
                time: tgtime
            };


            function writeString( _v_$ ) {

                var s = "";

                for (var i = 0; i < _v_$.length; i++) {

                    var v = _v_$[ i ];

                    v = ( v < 10 ) ? ( "0" + v ) : v;
                    v = ( v >= 59 ) ? 59 : v;
                    v = v.toString();

                    s += v + ".";
                }

                s = s.slice( 0, -1 );

                //console.log("STRING", s);

                return s;
            }

            return [ spoofKickoff, spoofTarget ];

            //"date":"17.07.2014",
            //"time":"18.05.30"

            /*
             yy      : n( _date_$[ 2 ] ),
             mm      : n( _date_$[ 1 ] ),
             dd      : n( _date_$[ 0 ] ),
             hour    : n( _time_$[ 0 ] ),
             mins    : n( _time_$[ 1 ] ),
             secs    : n( _time_$[ 2 ] )
             */
        };


        return {
            Sg_READY: Sg_READY,
            voKeys:voKeys,
            setData: setData,
            getVObyKey:getVObyKey
        }

    } );
