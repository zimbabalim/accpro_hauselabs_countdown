/**
 * Zimbabalim
 */
 "use strict";

define([ "Bootstrap", "ViewMediator" ], function( Bootstrap, ViewMediator ){


    var init = function(){

        ////console.log("/context/ -init");

        Bootstrap.Sg_COMPLETE.addOnce( startup );
        Bootstrap.init();

    };


    var startup = function(){

        ////console.log("/context/ -startup");

        ViewMediator.init();


        //TEST ONLY REMOVE

//        var o = {
//            v:0
//        };
//
//        TweenMax.to( o, 10, { v:100, onUpdate:function(){
//            //console.log("OOOOOO", o.v );
//        } });

    };


    return{
        init:init
    }

} );
