/**
 * Zimbabalim
 * entry point - sets up short cuts to libs and fires context.init on document ready
 */
"use strict";

(function(){

    require.config({

        baseUrl             :   "../src/_.js/src",
        paths : {
            signals         :   "../libs/signals.min",
            underscore      :   "../libs/underscore-min",
            TweenMax        :   "../libs/GreenSock-JS/src/minified/TweenMax.min",
            fastclick       :   "../libs/fastclick",
            Hammer          :   "../libs/hammer.min"
        }
    });

    require( [ "fastclick", "Context" ],
        function( fastclick, Context ){

            ////console.log("/main/ #");

            // TODO consider losing jquery
            $( document ).ready(function() {
                onDocumentReady();
            });


            function onDocumentReady(){

                ////console.log("/main/ -onDocumentReady");

                fastclick.attach( document.body );
                Context.init();
            }

        });

}());