/**
 * Zimbabalim
 */
"use strict";

define([ "signals" ], function( signals ){

    var events = {
        Sg_MOMENT_ACTIVATED     : new signals.Signal()
    };

    var momentVOs_$ = null,
        dateObjects_$ = null,
        numDateObjects = null;

    var init = function( _data ){

        momentVOs_$ = _data;
        dateObjects_$ = momentVOs_$.dateObjects_$;

        numDateObjects = dateObjects_$.length;

        ////console.log("/momentsmanager/ -init", momentVOs_$, dateObjects_$ );
    };

    var update = function( _props ){

        //////console.log("/momentsmanager/ -update", _props.dateObjectKey );

        compare( _props.dateObjectKey );
    };


    var compare = function( _dateObjKey ){

        for( var i = 0; i < numDateObjects; i ++ ){

            var dok = dateObjects_$[ i ];

            if( dok.dateObjectKey === _dateObjKey ){

                ////console.log("!!!!!!!!!MATCH", dok.dateObjectKey, _dateObjKey );

                dispatchMoment( i );

                break;
            }
        }
    };

    /**
     * DEBUG ONLY REMOVE
     * @param _i
     */
    window.m = function( _i ){
        dispatchMoment( _i );
    };


    var dispatchMoment = function( _index ){

        var momentVO = momentVOs_$[ _index ];

        if( !momentVO ){
            ////console.log("DEFEAT");
            return;
        }

        events.Sg_MOMENT_ACTIVATED.dispatch(
            {
                name: "Sg_MOMENT_ACTIVATED",
                data: momentVO
            }
        );
    };


    return {
        events:events,
        init:init,
        update:update
    }

} );
