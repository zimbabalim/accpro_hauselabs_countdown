/**
 * Zimbabalim
 */
"use strict";

define([ "signals", "Config", "utils/WriteDateObjKey" ],
    function( signals, Config, WriteDateObjKey ){

    var events = {

        Sg_TIME_ENGINE_UPDATE : new signals.Signal()
    };

    var targetDate = null, // NOTE final date
        kickoffDate = null, // NOTE date that timer should begin
        currDate = null, // NOTE date now
        evaluationDateProxy = null,// NOTE reference object, either targetDate || kickoffDate
        timer = null;

    var props = {
        days:null,
        hours:null,
        minutes:null,
        seconds:null,
        dateObjectKey:null,
        hasKickedOff:false,
        hasExpired:false
    };

    var init = function( _currTimeVO, _targetTimeVO, _kickoffTimeVO  ){

        // NOTE create new date object with current date props - may not be js client time so don't laugh
        var refDate = new Date( _currTimeVO.yy, _currTimeVO.mm - 1, _currTimeVO.dd, _currTimeVO.hour, _currTimeVO.mins, _currTimeVO.secs );

        kickoffDate = new Date( _kickoffTimeVO.yy, _kickoffTimeVO.mm - 1, _kickoffTimeVO.dd, _kickoffTimeVO.hour, _kickoffTimeVO.mins, _kickoffTimeVO.secs );
        targetDate = new Date( _targetTimeVO.yy, _targetTimeVO.mm - 1, _targetTimeVO.dd, _targetTimeVO.hour, _targetTimeVO.mins, _targetTimeVO.secs );

        currDate = new Date();
        currDate.setTime( refDate.getTime() );


        update();
        timer = window.setInterval( update, 1000 );
    };


    var update = function(){

        var cs = currDate.getSeconds();
        currDate.setSeconds( cs += 1 );

        /**
         * branch based on 'respectKickOffDate' boolean
         */
        if( Config.timeAndDateProps.respectKickOffDate ){

            /**
             * run 'kickoffdate' or 'targetdate' through calculator depending on value of 'props.hasKickedOff'
             */
            evaluationDateProxy = ( props.hasKickedOff ) ? targetDate : kickoffDate; // TEST ONLY
            calcTimeRemaining( evaluationDateProxy );

        } else {

            /**
             * just run 'targetdate' regardless
             * @type {boolean}
             */
            props.hasKickedOff = true;
            calcTimeRemaining( targetDate );
        }


        // *************************************
        events.Sg_TIME_ENGINE_UPDATE.dispatch( props );
        // *************************************
    };


    // TODO
    var calcTimeRemaining = function( _targetDate ){

        var seconds = Math.floor(( _targetDate - ( currDate )) / 1000),
            minutes = Math.floor( seconds / 60 ),
            hours = Math.floor( minutes / 60 ),
            days = Math.floor( hours / 24 );

        hours = hours - ( days * 24 );
        minutes = minutes - ( days * 24 * 60 ) - ( hours * 60 );
        seconds = seconds - ( days * 24 * 60 * 60 ) - ( hours * 60 * 60 ) - ( minutes * 60 );

        props.days = days;
        props.hours = hours;
        props.minutes = minutes;
        props.seconds = seconds;

        validate();

        props.dateObjectKey = WriteDateObjKey.createKey(
            {
                yy      : currDate.getFullYear(),
                mm      : currDate.getMonth() + 1,
                dd      : currDate.getDate(),
                hour    : currDate.getHours(),
                mins    : currDate.getMinutes(),
                secs    : currDate.getSeconds()
            }
        );
    };


    var validate = function(){

        var lessThan24Hours = ( props.days === 0 ),
            hasExpired = ( props.days === -1 );

        /**
         * it's all over
         */
        if( props.hasKickedOff && hasExpired ){

            //console.log( "***BINGO!!!***" );

            props.days = 0;
            props.hours = 0;
            props.minutes = 0;
            props.seconds = 0;

            props.hasExpired = true;
            window.clearInterval( timer );
        }

        /**
         * it's just begun
         */
        if( !props.hasKickedOff && hasExpired ){

            //console.log( "***KICKOFF!!!***" );
            props.hasKickedOff = true;
        }
    };

    return {
        events:events,
        init:init
    }

} );
