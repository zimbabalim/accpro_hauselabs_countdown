/**
 * Zimbabalim
 * constant values we can use throughout the app
 */
"use strict";

define([], function(){

    var paths = {
        images:null,
        data:"data/data.json",
        videoDir:"assets/video/"
//        video:"assets/video/"
    };

    var startupProps = {
        enableDebugQueryString:true, // NOTE if appending '?hlcddebug' and this value === true, show 'debug' panel
        alwaysCacheImages:true, // NOTE if degraded / mobile version do we cache images?
        forceMobileVersion:false // NOTE force mobile view (js only) for debugging - FIXIT for production set to FALSE!!!
    };


    /**
     * props set on startup used to determine app mode variables
     * @type {{isDaftClient: null, isMobile: null, useTouchEvents: null}}
     */
    var capabilities = {
        isIE8:null, // NOTE i.e. IE8...
        isMobile:null,
        likelyLowFps:null, // NOTE will be set to false if RequestAnimationFrame unavailable
        useTouchEvents:null
    };

    /**
     * viewport properties
     * @type {{dims: null, mobileBreakpoint: number, minWidth: number, maxWidth: number, maxHeight: number}}
     */
    var viewportProps = {
        dims:null, // NOTE object : {w, h}
//        mobileBreakpoint:640,//640,
//        minWidth:800, // NOTE point at which we will dispense with the circular carousel thang // FIXIT
//        maxWidth:1600, // NOTE point at which focalPoint/radius will stop extending // FIXIT
//        maxHeight:500 // FIXIT

        mobileBreakpoint:640,
        minWidthBeforeScaling:980
    };


    /**
     * set STARTUP time and date from some source
     * @type {{date: null, time: null}}
     */
    var timeAndDateProps = {

        method:"client", // NOTE method to use: "client" (system clock) || "server" (php on host) || "api" (some webservice)
//        currDate:null,
//        currTime:null,
        dateObject:null,

        respectKickOffDate:true, // NOTE wether or not count from now, or wait until kickoffDate

        timezoneOffset:null // NOTE not used
    };

    var clockMessageProps = {
        modulo:1, // NOTE filter updates by this value (performance tweak - note: will affect speed param effect)
        speed:3 // NOTE higher is faster, decimals will be rounded up - avoid
    };


    var kickoffAnimationProps = {
        maxTime:12
    };


    return {
        paths:paths,
        startupProps:startupProps,
        capabilities:capabilities,
        viewportProps:viewportProps,
        timeAndDateProps:timeAndDateProps,
        clockMessageProps:clockMessageProps,
        kickoffAnimationProps:kickoffAnimationProps
    }
} );
